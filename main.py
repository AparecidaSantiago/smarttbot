import argparse
from app import Grafico, Interface
from app.indicadores import MediaMovel, Ifr, Bollinger, Volume, Calculo, Posicao

parser = argparse.ArgumentParser(description="Desafio indicadores")
parser.add_argument('path', type=str, help="Caminho do arquivo csv")
parser.add_argument('--interface', dest='interface', action='store_const', const=1, help='Exibir gráfico no navegador')
parser.add_argument('-i', dest='inicio', type=str, help="Data inicial")
parser.add_argument('-f', dest='fim', type=str, help="Data final")
args = parser.parse_args()


def main():
    try:
        if args.interface:
            g = Interface(csv=args.path, intervalo=(args.inicio, args.fim))
        else:
            g = Grafico(csv=args.path, intervalo=(args.inicio, args.fim))
    except FileNotFoundError:
        print(f'O arquivo "{args.path}" não foi encontrado.')
        return

    g.add_indicador(MediaMovel(34, cor='#eedd44'))
    g.add_indicador(MediaMovel(20, Calculo.EXPONENCIAL, cor='#4444ee'))
    g.add_indicador(Ifr(14, Posicao.JANELA, cor='#9977bb'))
    g.add_indicador(Bollinger(25, cor='#505078'))
    g.add_indicador(Volume(Posicao.JANELA, cor='#eeeeee'))

    g.exibir()


if __name__ == "__main__":
    main()
