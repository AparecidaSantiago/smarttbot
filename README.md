
# Sobre o projeto
Este projeto foi desenvolvido com objetivo de implementar candlesticks utilizando indicadores pre definidos a partir de um dataset.

# Funcionalidades do projeto

- `Salvar CSV com os indicadores implementados`
- `Exibir grafico de candlesticks interativo com os indicadores`


# Setup e execução do projeto

Neste tópico iremos aprender a fazer o setup do nosso projeto.

### Sem Docker

## Requisitos

- Python 3.10
- pip
- Ubuntu 18.04+

Na sua pasta de projetos, execute:

- `git clone git@gitlab.com:AparecidaSantiago/smarttbot.git`
- `cd smarttbot`
- `python -m venv env`
- `source .env/bin/activate`
- `pip install -r requirements.txt`

Feito isso, suas dependências estão instaladas.

### Executando
Para visualizar os argumentos disponíveis:
```
python main.py -h
```
Exemplo de execução:
```
python main.py <caminho-do-arquivo> -i "2021-03-28 20:00:00" -f "2021-03-28 23:59:00"
```
**Observação importante**: Para visualização dos gráficos execute o comando passando também o argumento ``` --interface ```.
Isto fará com que um servidor rodando em http://127.0.0.1:8050/ seja iniciado. Outro ponto importante é que não foi implementada nenhuma otimização para renderização do gráfico e a ferramenta utilizada não mostra bom desempenho para um intervalo de dados grande. Assim é sugerido que utilize **intervalos de até um dia**.

Exemplo:
```
python main.py <caminho-do-arquivo> -i "2021-03-28 20:00:00" -f "2021-03-28 23:59:00" --interface
```

### Com Docker
O arquivo [bitstampUSD_1-min_data_2012-01-01_to_2021-03-31.csv](https://www.kaggle.com/datasets/mczielinski/bitcoin-historical-data/download?datasetVersionNumber=7) deve estar na pasta do projeto **ou** como opção usar o seguinte argumento no build do container: `--build-arg csv=arquivo.csv`

- Montar a imagem do container:
    - `docker build -t smarttbot .`
- **Opcional**: Informando o caminho do csv de entrada: 
    - `docker build -t smarttbot . --build-arg csv=arquivo.csv`

- Criar e executar o container informando o intervalo nos dados
    - `docker run --rm -it -v $PWD/out:/smarttbot/out smarttbot -i <data-inicial> -f <data_final>`

Exemplo:
- `docker run --rm -it -v $PWD/out:/smarttbot/out smarttbot -i "2021-03-28 20:00:00" -f "2021-03-28 23:59:00"`

Observação: O arquivo deve estar no mesmo diretório

## Decisões de arquitetura
A primeira decisão tomada foi quanto ao tratamento do CSV. Para isto foi analisado o desempenho na execução da leitura do arquivo utilizando a biblioteca padrão do Python para CSV versus a [Pandas](https://pandas.pydata.org/).

- csv
    - Executed in   11.50 secs    fish           external
   usr time   10.51 secs  644.00 micros   10.51 secs
   sys time    0.80 secs  733.00 micros    0.80 secs

- pandas
    - Executed in  601.80 millis    fish           external
   usr time    2.73 secs    412.00 micros    2.73 secs
   sys time    1.34 secs      0.00 micros    1.34 secs


- **Pandas**:\
Dado o tamanho do dataset também foi utilizado o [Pyarrow](https://arrow.apache.org/docs/python/index.html) para gereciar melhor os dados em memória.\
Desta forma conseguiu-se uma melhoria significante na performance. A Pandas é uma plataforma de desenvolvimento pra ciência de dados e é perfeita para manipulação desse conjunto de dados pra análise financeira.

A segunda decisão foi quanto a implementação dos indicadores. Nesse caso a decisão era implementá-los por completo ou utilizar uma biblioteca que auxilie na implementação. Decidiu-se por utilizar os recursos da Pandas para implementar os indicadores.

Por último a decisão da ferramenta para plotagem dos gráficos. Decidiu-se utiliza a [Ploty](https://dash.plotly.com/) que possibilita a renderização dos gráficos com JS no navegador através de um servidor com Flask no backend.

## Testes unitários #TODO
Não foram implementados testes unitários. Mas o objetivo era utilizar a lib PyTest e também uma ferramenta de qualidade de código como o prospector.
