FROM python:3.10-slim-bullseye

ARG csv=./bitstampUSD_1-min_data_2012-01-01_to_2021-03-31.csv
WORKDIR /smarttbot

COPY app/ app/
COPY main.py requirements.txt ./
COPY ${csv} btcusd.csv

RUN pip install -r requirements.txt
EXPOSE 8050
ENTRYPOINT ["python", "./main.py", "btcusd.csv"]