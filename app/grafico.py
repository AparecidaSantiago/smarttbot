from .indicadores import Indicador
from .config import *
from datetime import datetime
from pandas import DataFrame
import pandas, os


class Grafico():
    def __init__(
        self,
        csv: str = None,
        intervalo: tuple = (),
        nome = 'Gráfico'
    ):
        self.csv = csv
        self.intervalo = intervalo
        self.nome = nome
        self.dados: DataFrame
        self.lacunas: DataFrame
        self.indicadores: list[Indicador] = []

        if csv:
            self.carregar_dados(csv)


    def carregar_dados(self, csv: str):
        dados = pandas.read_csv(csv, engine='pyarrow')

        dados[DATA] = pandas.to_datetime(dados[DATA], unit='s')
        dados.set_index(DATA, inplace=True)

        inicio = self.intervalo[0] if self.intervalo[0] else dados.index[0]
        fim = self.intervalo[1] if self.intervalo[1] else dados.index[-1]

        intervalo = pandas.date_range(start=inicio, end=fim, freq='min')
        dados = dados[dados.index.isin(intervalo)]

        self.lacunas = dados[dados.isnull().values.any(axis=1)].index
        self.dados = dados.dropna()


    def exibir(self, so_indicadores=True):
        timestamp = datetime.now().strftime("%d-%m-%Y %H-%M-%S")
        colunas = [DATA] + self.dados.columns.names

        if so_indicadores:
            colunas = [DATA]

            for ind in self.indicadores:
                for col in ind.dados:
                    colunas.append(col)

        # para ter a data no formato unix timestamp
        self.dados[DATA] = self.dados.index
        self.dados[DATA] = self.dados[DATA].astype(int) // 10**9

        arquivo = os.path.join('out', f"{self.nome}_{timestamp}.csv")

        try:
            print(f'Criando arquivo "{arquivo}"...')
            self.dados[colunas].to_csv(arquivo, index=False, na_rep='NaN')
        except OSError:
            print('\n...Erro!\n\nVocê provavelmente rodou o container sem definir um'
                  ' volume para a saída dos dados.\n\nUse "-v $PWD/out:/smarttbot/out".\n')

        self.dados.pop(DATA)


    def add_indicador(self, indicador: Indicador):
        indicador.carregar_dados(self.dados)

        self.indicadores.append(indicador)
