from enum import Enum, auto
from pandas import DataFrame


class Calculo(Enum):
    ARITMETICO = auto()
    EXPONENCIAL = auto()


class Posicao(Enum):
    GRAFICO = auto()
    JANELA = auto()


class Indicador():
    def __init__(
        self,
        periodo: int = 0,
        posicao = Posicao.GRAFICO,
        cor: str = '#ffffff'
    ):
        self.periodo = periodo
        self.posicao = posicao
        self.cor = cor
        self.dados: DataFrame
        self.objeto: dict
        self.nome = 'indicador'


    def carregar_dados(self, dados: DataFrame) -> DataFrame:
        pass

    def interface_config(self, col: str, config: dict) -> dict:
        pass
