from .indicador import Indicador, Posicao
from ..config import *
from pandas import DataFrame


class Volume(Indicador):
    def __init__(
        self,
        posicao = Posicao.JANELA,
        cor: str = '#ffffff'
    ):
        super().__init__(posicao=posicao, cor=cor)

        self.objeto = 'bar'
        self.nome = 'Volume'


    def carregar_dados(self, dados: DataFrame) -> DataFrame:
        dados[self.nome] = dados[VOLUME]
        self.dados = dados[[self.nome]]

        return self.dados
