from .indicador import Indicador, Posicao
from ..config import *
from pandas import DataFrame
from plotly import colors


class Bollinger(Indicador):
    def __init__(
        self,
        periodo: int,
        posicao = Posicao.GRAFICO,
        cor: str = '#ffffff'
    ):
        super().__init__(periodo, posicao, cor)

        self.objeto = 'scatter'
        self.nome = 'Bollinger'


    def carregar_dados(self, dados: DataFrame) -> DataFrame:
        soma = dados[CLOSE].rolling(self.periodo)
        mma = soma.mean()
        desvio = soma.std(ddof=0) * 2

        bandas = [mma + desvio, mma - desvio]
        banda_a = f"{self.nome}-A"
        banda_b = f"{self.nome}-B"

        dados[banda_a] = bandas[0]
        dados[banda_b] = bandas[1]
        self.dados = dados[[banda_a, banda_b]]

        return self.dados


    def interface_config(self, col: str, config: dict) -> dict:
        banda_a = f"{self.nome}-A"
        banda_b = f"{self.nome}-B"

        colunas = {
            banda_a: {
                'name': self.nome,
                'legendgroup': self.nome,
            },
            banda_b: {
                'name': self.nome,
                'legendgroup': self.nome,
                'showlegend': False,
                'fill': 'tonexty',
                'fillcolor': 'rgba(80,80,120, 0.25)',
            }
        }

        return colunas[col]
