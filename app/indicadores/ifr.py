from .indicador import Indicador, Posicao
from ..config import *
from pandas import DataFrame


class Ifr(Indicador):
    def __init__(
        self,
        periodo: int,
        posicao = Posicao.GRAFICO,
        cor: str = '#ffffff'
    ):
        super().__init__(periodo, posicao, cor)

        self.objeto = 'scatter'
        self.nome = 'IFR'


    def carregar_dados(self, dados: DataFrame) -> DataFrame:
        variacao = dados[CLOSE].diff()
        ganhos = variacao.mask(variacao < 0, 0.0)
        perdas = -variacao.mask(variacao > 0, -0.0)

        media_ganhos = ganhos.ewm(min_periods=self.periodo, alpha=1 / self.periodo).mean()
        media_perdas = perdas.ewm(min_periods=self.periodo, alpha=1 / self.periodo).mean()

        fr = media_perdas / media_ganhos
        ifr = (100 - (100 / (1 + fr)))

        dados[self.nome] = ifr
        self.dados = dados[[self.nome]]

        return self.dados
