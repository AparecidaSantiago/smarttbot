from .indicador import Indicador, Calculo, Posicao
from .media_movel import MediaMovel
from .volume import Volume
from .ifr import Ifr
from .bollinger import Bollinger
