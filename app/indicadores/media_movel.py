from .indicador import Indicador, Calculo, Posicao
from ..config import *
from pandas import DataFrame


class MediaMovel(Indicador):
    def __init__(
        self,
        periodo: int,
        calculo: Calculo = Calculo.ARITMETICO,
        posicao = Posicao.GRAFICO,
        cor: str = '#ffffff'
    ):
        super().__init__(periodo, posicao, cor)

        self.calculo = calculo
        self.objeto = 'scatter'
        self.nome = 'Média Móvel'


    def carregar_dados(self, dados: DataFrame) -> DataFrame:
        if self.calculo == Calculo.ARITMETICO:
            self.nome = 'MMA'
            media = self.mma(dados)
        elif self.calculo == Calculo.EXPONENCIAL:
            self.nome = 'MME'
            media = self.mme(dados)

        dados[self.nome] = media
        self.dados = dados[[self.nome]]

        return self.dados


    def mma(self, dados: DataFrame):
        return dados[CLOSE].rolling(self.periodo).mean()


    def mme(self, dados: DataFrame):
        return dados[CLOSE].ewm(span=self.periodo, min_periods=self.periodo).mean()
