from app import Grafico
from .indicadores import Indicador, Posicao
from .config import *
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from dash import Dash, dcc, html


class Interface(Grafico):
    def __init__(
        self,
        csv,
        intervalo = (),
        nome = 'Gráfico'
    ):
        super().__init__(csv, intervalo, nome)

        self.figure: go.Figure
        self.dash: Dash
        self.grafico: list[Indicador] = []
        self.janela: list[Indicador] = []


    def add_indicador(self, indicador: Indicador):
        super().add_indicador(indicador)

        if indicador.posicao == Posicao.GRAFICO:
            self.grafico.append(indicador)
        else:
            self.janela.append(indicador)


    def exibir(self):
        self.configurar()

        eixo_y = 1
        candlestick = self.criar_grafico(eixo_y)
        self.figure.add_trace(candlestick)

        for ind in self.grafico:
            self.criar_objeto(ind, eixo_y)

        for ind in self.janela:
            eixo_y += 1
            self.criar_objeto(ind, eixo_y)

        self.dash.run_server(debug=True)


    def configurar(self):
        self.dash = Dash(self.nome)
        self.figure = make_subplots(
            shared_xaxes=True,
            rows=3, cols=1,
            subplot_titles=('a', 'b', 'c'),
            row_heights=[0.74, 0.13 , 0.13],
            vertical_spacing=0
        )

        graph_config = {
            'locale': 'pt-BR',
            'scrollZoom': True,
            'displaylogo': False,
            'modeBarButtonsToRemove': ['select', 'zoom2d', 'lasso2d'],
            'displayModeBar': True,
            'fillFrame': True
        }

        figure_config = {
            'title': 'Bitcoin 1 min',
            'xaxis_rangeslider_visible': False,
            'paper_bgcolor': '#ffffff',
            'plot_bgcolor': '#1E2428',
            'dragmode': 'pan',
            'xaxis': {'rangebreaks': [{'values': self.lacunas, 'dvalue': 60000}], 'showticklabels': False, 'showgrid': False, 'tickformat': '%d/%m/%Y %H:%M'},
            'yaxis3': {'showticklabels': False, 'fixedrange': True, 'showgrid': False},
            'yaxis2': {'showticklabels': False, 'fixedrange': True, 'showgrid': False},
            'yaxis': {'showgrid': False, 'tickformat': ',.2f', 'side': 'right'},
            'margin': {'b': 40, 'l': 50, 'r': 100, 't': 40, 'pad': 4},
            'legend': {'x': 0}
        }

        self.figure.update_layout(figure_config)
        self.figure.update_xaxes(row=3, col=1, showgrid=False)
        self.figure.update_xaxes(row=2, col=1, showgrid=False)
        self.figure.layout.annotations[0].update(text='')

        self.dash.layout = html.Div([
            dcc.Graph(id='graph', config=graph_config, figure=self.figure)
        ])


    def criar_grafico(self, eixo_y: int):
        return {
            'name': "BTC 1 min",
            'type': 'candlestick',
            'x':  self.dados.index,
            'yaxis': 'y'+str(eixo_y or ''),
            'open':  self.dados[OPEN],
            'high':  self.dados[HIGH],
            'low':  self.dados[LOW],
            'close':  self.dados[CLOSE],
            'showlegend': False,
            'xhoverformat': "%d/%m/%Y %H:%M",
            'yhoverformat': ",.2f",
            'increasing': {"line": {"color": "#089981", "width": 1}, "fillcolor": "#089981"},
            'decreasing': {"line": {"color": "#f23645", "width": 1}, "fillcolor": "#f23645"},
        }


    def criar_objeto(self, indicador: Indicador, eixo_y: int):
        for col in indicador.dados:
            objeto = {
                'name': col,
                'type': indicador.objeto,
                'x': self.dados.index,
                'y': indicador.dados[col],
                'yaxis': 'y'+str(eixo_y or ''),
                'marker': {'color': indicador.cor},
                'xhoverformat': "%d/%m/%Y %H:%M",
                'yhoverformat': ",.2f",
            }

            config = indicador.interface_config(col, objeto)

            if config:
                objeto.update(config)

            self.figure.add_trace(objeto, row=eixo_y , col=1)

            if indicador in self.janela:
                self.figure.layout.annotations[eixo_y-1].update(
                    text=col,
                    font={'color': 'white'},
                    bgcolor='rgba(255,255,255,0.2)',
                    xanchor='left',
                    yanchor='top',
                    width=80,
                    align='left',
                    x=0
                )
